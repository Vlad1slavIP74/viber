import { papyrus } from './papyrus.mjs'
import { commands, menus } from './markup.mjs'

const contextTree = [
  {
    scope: commands.toShockingFacts,
    previousScope: commands.toShockingFacts,
    keyboard: menus.intro,
    papyrus: papyrus.getShokingFacts,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.toExpandedTree,
    previousScope: commands.toExpandedTree,
    keyboard: menus.toExpandedTree,
    papyrus: papyrus.toExpandedTree,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.ourServices,
    previousScope: commands.toExpandedTree,
    keyboard: menus.ourServices,
    papyrus: papyrus.ourServices,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.askQuestion,
    previousScope: commands.toShockingFacts,
    keyboard: menus.toExpandedTree,
    papyrus: papyrus.getAskQuestion,
    inputMethod: ['keyboard'],
  },
  {
    scope: commands.askName,
    previousScope: commands.askQuestion,
    keyboard: menus.risks,
    papyrus: papyrus.getName,
    inputMethod: ['keyboard'],
  },
  {
    scope: commands.askPhone,
    previousScope: commands.askName,
    keyboard: menus.risks,
    papyrus: papyrus.getPhone,
    inputMethod: ['keyboard'],
  },
  {
    scope: commands.servicesCatalog,
    previousScope: commands.ourServices,
    keyboard: menus.servicesCatalog,
    papyrus: papyrus.servicesCatalog,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.pricesTerms,
    previousScope: commands.ourServices,
    keyboard: menus.pricesTerms,
    papyrus: papyrus.pricesTerms,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.contactUs,
    previousScope: commands.expandedTree,
    keyboard: menus.contactUs,
    papyrus: papyrus.contactUs,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.hostingSupport,
    previousScope: commands.ourServices,
    keyboard: menus.hostingSupport,
    papyrus: papyrus.hostingSupport,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.usingStrategies,
    previousScope: commands.toExpandedTree,
    keyboard: menus.usingStrategies,
    papyrus: papyrus.ourServices,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.applianceSpheres,
    previousScope: commands.usingStrategies,
    keyboard: menus.hostingSupport,
    papyrus: papyrus.hostingSupport,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.benefits,
    previousScope: commands.usingStrategies,
    keyboard: menus.benefits,
    papyrus: papyrus.benefits,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.risks,
    previousScope: commands.risks,
    keyboard: menus.risks,
    papyrus: papyrus.risks,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.blog,
    previousScope: commands.contactUs,
    keyboard: menus.blog,
    papyrus: papyrus.blog,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.contacts,
    previousScope: commands.contactUs,
    keyboard: menus.contacts,
    papyrus: papyrus.contacts,
    inputMethod: ['buttons'],
  },
]

class Context {
  constructor() {
    this.events = {}
    this.ctx = {
      scope: null,
      previousScope: null,
      question: null,
      name: null,
      phone: null,
      contactWay: null,
    }
    this.tree = contextTree
  }

  getContext(property = false) {
    if (property) {
      if (Reflect.has(this.ctx, property)) return this.ctx[property]
      return null
    }
    return this.ctx
  }

  setContext(obj) {
    Object.keys(obj).forEach(item => {
      if (Reflect.has(this.ctx, item)) return (this.ctx[item] = obj[item])
      return null
    })
  }

  clearContext() {
    Object.keys(this.ctx).forEach(item => (this.ctx[item] = null))
  }

  allowUserInput(textStatus = false) {
    this.ctx.commandsOnly = !textStatus
  }

  checkUserInputAvailability(scope) {
    if (!scope) return null
    const inputAvailability = this.tree.find(node => node.scope === scope).inputMethod
    return inputAvailability || null
  }

  getNode(scope) {
    if (!scope) return null
    return this.tree.find(node => node.scope === scope) || null
  }

  getParentNode() {
    if (!this.ctx.previousScope) return null
    return this.tree.find(node => node.scope === this.ctx.previousScope) || null
  }

  getAll() {
    return this.ctx
  }
}
export const ctxManager = new Context()
