import { KeyboardGenerator } from './KeyboardGenerator.mjs'

export const commands = {
  findOutMore: 'Узнать больше',
  getStarted: 'BOOTBOT_GET_STARTED',
  faq: 'FAQ',
  toShockingFacts: 'toShockingFacts',
  toExpandedTree: 'toExpandedTree',
  moreInformation: 'MORE_INFORMATION',
  stopPropagation: 'STOP_PROPAGATION',
  ourServices: 'ourServices',
  servicesCatalog: 'servicesCatalog',
  hostingSupport: 'hostingSupport',
  usingStrategies: 'usingStrategies',
  applianceSpheres: 'applianceSpheres',
  benefits: 'benefits',
  risks: 'risks',
  askQuestion: 'ask question',
  askPhone: 'ASK_PHONE',
  requireQuestion: 'REQUIRE_QUESTION',
  askName: 'ASK_NAME',
  contactUs: 'contactUs',
  getConsultation: 'GET_CONSULTATION',
  confirmConsultation: 'CONFIRM_CONSULTATION',
  refuseConsultation: 'REFUSE_CONSULTATION',
  contactsInfo: 'CONTACTS_INFO',
  blog: 'blog',
  pricesTerms: 'pricesTerms',
  confirmQuestion: 'CONFIRM_QUESTION',
  refuseQuestion: 'REFUSE_QUESTION',
  goBack: 'goBack',
  contacts: 'Contacts',
}

export const buttons = {
  initialKeyboard: () => [
    {
      text: 'Interesting facts',
      actionBody: commands.toShockingFacts,
      cols: 6,
    },
  ],
  contextTree: () => [
    {
      text: 'Optional tree',
      actionBody: commands.toExpandedTree,
      cols: 3,
    },
    {
      text: 'go back',
      actionBody: commands.goBack,
      cols: 3,
    },
  ],
  expandedTree: () => [
    {
      text: '⚙️Наши услуги',
      actionBody: commands.ourServices,
      cols: 3,
    },
    {
      text: '📊Стратегии использования ботов',
      actionBody: commands.usingStrategies,
      cols: 3,
    },
    {
      text: '🛰Связаться с нами',
      actionBody: commands.contactUs,
      cols: 3,
    },
    {
      text: '❓Задать вопрос',
      actionBody: commands.askQuestion,
      cols: 3,
    },
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  ourServices: () => [
    {
      text: '📂Каталог услуг',
      actionBody: commands.servicesCatalog,
      cols: 3,
    },
    {
      text: '🧾Цены и сроки выполнения',
      actionBody: commands.pricesTerms,
      cols: 3,
    },
    {
      text: '🛠Поддержка и хостинг',
      actionBody: commands.hostingSupport,
      cols: 3,
    },
    {
      text: '❓Задать вопрос',
      actionBody: commands.askQuestion,
      cols: 3,
    },
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  usingStrategies: () => [
    {
      text: '🧪Сферы применения',
      actionBody: commands.applianceSpheres,
      cols: 3,
    },
    {
      text: '💪Преимущества',
      actionBody: commands.benefits,
      cols: 3,
    },
    {
      text: '🤞Недостатки и риски',
      actionBody: commands.risks,
      cols: 3,
    },
    {
      text: '❓Задать вопрос',
      actionBody: commands.askQuestion,
      cols: 3,
    },
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  contactUs: () => [
    {
      text: '📒Контакты',
      actionBody: commands.contacts,
      cols: 6,
    },
    {
      text: '📇Блог',
      actionBody: commands.blog,
      cols: 3,
    },
    {
      text: '❓Задать вопрос',
      actionBody: commands.askQuestion,
      cols: 3,
    },
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  servicesCatalog: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  pricesTerms: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  hostingSupport: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  applianceSpheres: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  benefits: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  risks: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  contacts: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
  blog: () => [
    {
      text: '◀️Назад',
      actionBody: commands.goBack,
      cols: 6,
    },
  ],
}

export const menus = {
  intro: KeyboardGenerator(buttons.initialKeyboard()),
  tree: KeyboardGenerator(buttons.contextTree()),
  toExpandedTree: KeyboardGenerator(buttons.expandedTree()),
  ourServices: KeyboardGenerator(buttons.ourServices()),
  usingStrategies: KeyboardGenerator(buttons.usingStrategies()),
  contactUs: KeyboardGenerator(buttons.contactUs()),
  servicesCatalog: KeyboardGenerator(buttons.servicesCatalog()),
  pricesTerms: KeyboardGenerator(buttons.pricesTerms()),
  hostingSupport: KeyboardGenerator(buttons.hostingSupport()),
  applianceSpheres: KeyboardGenerator(buttons.applianceSpheres()),
  benefits: KeyboardGenerator(buttons.benefits()),
  contacts: KeyboardGenerator(buttons.contacts()),
  blog: KeyboardGenerator(buttons.blog()),
  risks: KeyboardGenerator(buttons.risks()),
}
