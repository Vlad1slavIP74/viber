import axios from 'axios'
import dotenv from 'dotenv'
import { ctxManager, definiteLogger } from '../helpers/index.mjs'

dotenv.config()

const instance = axios.create({
  baseURL: process.env.API_HOST,
  headers: { Authorization: `Bearer ${process.env.API_TOKEN}` },
})

export const saveQuestion = async () => {
  try {
    const context = ctxManager.getContext()
    const data = {
      name: context.name,
      phoneNumber: context.phone,
      question: context.question,
      status: 'new',
    }
    await instance.post('/question', data)
  } catch (err) {
    definiteLogger('err')
  }
}
