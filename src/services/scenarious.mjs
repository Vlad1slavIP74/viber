import { papyrus, definiteLogger, ctxManager, commands, menus } from '../helpers/index.mjs'
import { saveQuestion } from './inquisitive.mjs'

const initial = async (onFinish, Message, name) => {
  ctxManager.setContext({
    scope: commands.toShockingFacts,
    previousScope: ctxManager.getContext('scope'),
    name,
  })
  return onFinish(new Message.Text(papyrus.getGreeting(name), menus.intro))
}

const interestingFacts = async (response, Message) => {
  try {
    await ctxManager.setContext({
      scope: commands.toShockingFacts,
      previousScope: ctxManager.getContext('scope'),
    })
    await response.send(new Message.Text(papyrus.getShokingFacts().text, menus.tree))
    // await response.send(fn(papyrus.getShokingFacts(), menus.tree))
  } catch (e) {
    logger.info(e)
  }
}

const goBack = async (response, Message) => {
  const parentNode = ctxManager.getParentNode()
  if (parentNode) {
    ctxManager.setContext({
      scope: parentNode.scope,
      previousScope: parentNode.previousScope,
    })
  }
  response.send(new Message.Text('Lets go back', parentNode.keyboard))
}

const askQuestion = async (response, Message) => {
  try {
    if (commands.askQuestion !== ctxManager.getContext('scope')) {
      await ctxManager.setContext({
        scope: commands.askQuestion,
        previousScope: ctxManager.getContext('scope'),
      })
    }
    if (ctxManager.getContext('name') && ctxManager.getContext('phone'))
      response.send(new Message.Text(papyrus.askQuestion))
    if (!ctxManager.getContext('name')) {
      await ctxManager.setContext({
        scope: commands.askName,
        previousScope: ctxManager.getContext('scope'),
      })
      response.send(new Message.Text(papyrus.askName))
    }
    if (!ctxManager.getContext('phone') && ctxManager.getContext('name')) {
      await ctxManager.setContext({
        scope: commands.askPhone,
        previousScope: ctxManager.getContext('scope'),
      })
      return response.send(new Message.Text(papyrus.askPhone))
    }
  } catch (e) {
    logger.info(e)
  }
}

const setName = async (message, response, Message) => {
  await ctxManager.setContext({
    name: message,
    scope: commands.askPhone,
    previousScope: ctxManager.getContext('scope'),
  })

  response.send(new Message.Text(papyrus.askPhone))
}

const setPhone = async (message, response, Message) => {
  await ctxManager.setContext({
    phone: message,
    scope: commands.askQuestion,
    previousScope: ctxManager.getContext('scope'),
  })

  response.send(new Message.Text(papyrus.askQuestion))
}

const setQuestion = async (message, response, Message) => {
  await ctxManager.setContext({
    scope: commands.askQuestion,
    question: message,
  })
  const res = saveQuestion()
  // PENDING ???????
  await response.send(
    new Message.Text(
      res ? papyrus.getSuccessQuestion : papyrus.errorOnQuestion,
      menus.toExpandedTree,
    ),
  )
  // for keys when user staart typing after asking
  await ctxManager.setContext({
    scope: commands.toExpandedTree,
    previousScope: commands.toExpandedTree,
  })
}
const BadBoy = async (response, Message) => {
  // eslint-disable-next-line no-unused-vars
  const { scope } = ctxManager.getContext()
  const menu = menus[scope] || menus.tree
  response.send(new Message.Text(papyrus.errorForTyping, menu))
}
// const optionalTree = async (response, Message) => {
//   try {
//     await ctxManager.setContext({
//       scope: commands.toExpandedTree,
//       previousScope: ctxManager.getContext('scope'),
//     })
//     await response.send(new Message.Text(papyrus.getExpandedTree(), menus.expandedTree))
//   } catch (e) {
//     logger.info(e)
//   }
// }
// const ourServices = async (response, Message) => {
//   try {
//     ctxManager.setContext({
//       scope: commands.ourServices,
//       previousScope: ctxManager.getContext('scope'),
//     })
//     await response.send(new Message.Text(papyrus.getOurServices(), menus.ourServices))
//   } catch (e) {
//     logger.info(e)
//   }
// }
// const usingStrategies = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.usingStrategies,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.getUsingStrategies(), menus.getUsingStrategies))
// }

// const contactUs = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.contactUs,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.contactUs(), menus.contactUs))
// }

// const servicesCatalog = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.servicesCatalog,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.servicesCatalog(), menus.servicesCatalog))
// }

// const pricesTerms = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.pricesTerms,
//     previousScope: ctxManager.getContext('scope'),
//   })

//   response.send(new Message.Text(papyrus.pricesTerms(), menus.pricesTerms))
// }

// const hostingSupport = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.hostingSupport,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.hostingSupport(), menus.hostingSupport))
// }

// const applianceSpheres = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.applianceSpheres,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.applianceSpheres(), menus.applianceSpheres))
// }

// const benefits = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.benefits,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.benefits(), menus.applianceSpheres))
// }

// const risks = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.risks,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.risks(), menus.risks))
// }

// const blog = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.blog,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.blog, menus.blog))
// }

// const contacts = async (response, Message) => {
//   await ctxManager.setContext({
//     scope: commands.contacts,
//     previousScope: ctxManager.getContext('scope'),
//   })
//   response.send(new Message.Text(papyrus.contacts, menus.contacts))
// }

// eslint-disable-next-line no-unused-vars
// async function Factory(param) {
//   console.log(papyrus[param])
//   return async (response, Message) => {
//     console.log(papyrus.param)
//     await ctxManager.setContext({
//       scope: commands.param,
//       previousScope: ctxManager.getContext('scope'),
//     })
//     response.send(new Message.Text(papyrus[param], menus[param]))
//   }
// }

// TODO add to check typeof papyrus.[param]
const Factory2 = param => {
  // eslint-disable-next-line no-shadow
  return async (response, Message) => {
    await ctxManager.setContext({
      scope: commands[param],
      previousScope: ctxManager.getContext('scope'),
    })
    response.send(new Message.Text(papyrus[param], menus[param]))
  }
}
export const scenarious = new Map()

scenarious.set('initial', initial)
scenarious.set('goBack', goBack)
scenarious.set('ask question', askQuestion)
scenarious.set('setName', setName)
scenarious.set('setPhone', setPhone)
scenarious.set('setQuestion', setQuestion)
scenarious.set('toShockingFacts', interestingFacts)
// try magic
scenarious.set('benefits', Factory2('benefits'))
scenarious.set('risks', Factory2('risks'))
scenarious.set('blog', Factory2('blog'))
scenarious.set('Contacts', Factory2('contacts'))
scenarious.set('toExpandedTree', Factory2('toExpandedTree'))
scenarious.set('ourServices', Factory2('ourServices'))
scenarious.set('usingStrategies', Factory2('usingStrategies'))
scenarious.set('contactUs', Factory2('contactUs'))
scenarious.set('servicesCatalog', Factory2('servicesCatalog'))
scenarious.set('pricesTerms', Factory2('pricesTerms'))
scenarious.set('hostingSupport', Factory2('hostingSupport'))
scenarious.set('applianceSpheres', Factory2('applianceSpheres'))
scenarious.set('BadBoy', BadBoy)
