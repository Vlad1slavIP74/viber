import express from 'express'
import dotenv from 'dotenv'
import ViberBot from 'viber-bot'
import { scenarious } from './services/scenarious.mjs'
import { validation, definiteLogger } from './helpers/index.mjs'
import { logger } from './helpers/logger.mjs'
import { ctxManager } from './helpers/context.mjs'
import { papyrus } from './helpers/papyrus.mjs'

dotenv.config()

const { Bot, Events, Message } = ViberBot
dotenv.config()

const app = express()
const bot = new Bot({
  definiteLogger,
  authToken: process.env.BOT_ACCOUNT_TOKEN,
  name: 'TryBots',
  avatar: null,
})

bot.onConversationStarted((userProfile, isSubscribed, context, onFinish) => {
  try {
    scenarious.has('initial') && scenarious.get('initial')(onFinish, Message, userProfile.name)
  } catch (e) {
    logger.info(e.stack())
  }
})

bot.on(Events.MESSAGE_RECEIVED, (message, response) => {
  try {
    // if (scenarious.has(message.text)) {
    //   scenarious.get(message.text)(response, Message)
    // } else {
    //   // cheking for users input
    //   if (validation.isName(message.text)) {
    //     scenarious.get('setName')(message.text, response, Message)
    //   }
    //   if (validation.isPhoneNumber(message.text)) {
    //     return scenarious.get('setPhone')(message.text, response, Message)
    //   }
    //   if (validation.isCustomQuestion(message.text)) {
    //     scenarious.get('setQuestion')(message.text, response, Message)
    //   }
    //   if (validation.unexpectedText(message.text)) scenarious.get('BadBoy')(response, Message)
    // }

    const scope = ctxManager.getContext('scope')

    if (
      scope === 'ASK_NAME' ||
      scope === 'ASK_PHONE' ||
      scope === 'ask question' ||
      scenarious.has(message.text)
    ) {
      if (scope === 'ASK_NAME') {
        validation.isName(message.text)
          ? scenarious.get('setName')(message.text, response, Message)
          : response.send(new Message.Text(papyrus.invalidName))
      }
      if (scope === 'ASK_PHONE') {
        validation.isPhoneNumber(message.text)
          ? scenarious.get('setPhone')(message.text, response, Message)
          : response.send(new Message.Text(papyrus.invalidPhoneNumber))
      }

      if (scope === 'ask question') {
        validation.isCustomQuestion(message.text)
          ? scenarious.get('setQuestion')(message.text, response, Message)
          : response.send(new Message.Text(papyrus.questionLimitation))
      }

      if (scenarious.has(message.text)) {
        scenarious.get(message.text)(response, Message)
      }
    } else {
      scenarious.get('BadBoy')(response, Message)
    }
  } catch (e) {
    definiteLogger(e)
  }
})

app.use('/viber/webhook', bot.middleware())

app.listen(process.env.PORT, () => {
  bot.setWebhook(`${process.env.URL}/viber/webhook`)
})
